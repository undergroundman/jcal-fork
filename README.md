jcal
====

Jalali calendar is a small and portable free software library to manipulate date and time in Jalali calendar system.
It's written in C and has absolutely zero dependencies. It works on top of any POSIX.1-2001 (and later) compatible
libc implementations. Jalali calendar provides an API similar to that of libc's timezone, date and time functions.

Jalali calendar package consists of a library namely libjalali and two simple and easy to use terminal tools, jcal
and jdate with functionality similar to UNIX cal and date.

***

note on undergroundman fork(edition)  
---
I only change these files:  
 1. /sources/libjalali/jtime.c
 2. /sources/src/jdate.h
 3. this readme file
 4. add folder archlinux and it's content  
and all of other files(src,copy right note, makefile and --- ) ___not___ modified  

I add two format:  
    --* %f year in farsi(utf-8)  
    --* %i day of month in farsi (utf-8)  
